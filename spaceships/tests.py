import datetime

from django.test import TestCase
from django.utils import timezone

from .models import ShipType, Spaceship, Booking


class SpaceshipModelTests(TestCase):

    def test_is_available_with_no_booking(self):
        """
        is_available() returns True for spaceship without any booking.
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=2)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            True)

    def test_is_available_with_booking_before(self):
        """
        is_available() returns True for spaceship with a booking before the 
        specified date time range and no booking overlapping it.
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=-2)
        booking_to_date = timezone.now() + datetime.timedelta(days=-1)
        booking = Booking.objects.create(
            title="Booking 1",
            spaceship=spaceship,
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=2)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            True)

    def test_is_available_with_booking_after(self):
        """
        is_available() returns True for spaceship with a booking after the 
        specified date time range and no booking overlapping it.
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=3)
        booking_to_date = timezone.now() + datetime.timedelta(days=4)
        booking = Booking.objects.create(
            title="Booking 1", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=2)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            True)

    def test_is_available_with_bookings_before_and_after(self):
        """
        is_available() returns True for spaceship with a booking before the 
        specified date time range and one after but no booking overlapping it.
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=-2)
        booking_to_date = timezone.now() + datetime.timedelta(days=-1)
        booking_before = Booking.objects.create(
            title="Booking before", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        booking_from_date = timezone.now() + datetime.timedelta(days=3)
        booking_to_date = timezone.now() + datetime.timedelta(days=4)
        booking_after = Booking.objects.create(
            title="Booking after", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=2)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            True)

    def test_is_available_with_booking_starting_before_ending_during(self):
        """
        is_available() returns False for spaceship with a booking overlapping
        the specified date time range (starts before and ends during it).
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=-2)
        booking_to_date = timezone.now() + datetime.timedelta(days=2)
        booking = Booking.objects.create(
            title="Booking 1", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=3)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            False)

    def test_is_available_with_booking_starting_before_ending_after(self):
        """
        is_available() returns False for spaceship with a booking overlapping
        the specified date time range (starts before and ends after it).
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=-2)
        booking_to_date = timezone.now() + datetime.timedelta(days=4)
        booking = Booking.objects.create(
            title="Booking 1", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=3)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            False)

    def test_is_available_with_booking_starting_during_ending_after(self):
        """
        is_available() returns False for spaceship with a booking overlapping
        the specified date time range (starts during and ends after it).
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=2)
        booking_to_date = timezone.now() + datetime.timedelta(days=4)
        booking = Booking.objects.create(
            title="Booking 1", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=3)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            False)

    def test_is_available_with_booking_starting_during_ending_during(self):
        """
        is_available() returns False for spaceship with a booking overlapping
        the specified date time range (starts during and ends during it).
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=2)
        booking_to_date = timezone.now() + datetime.timedelta(days=3)
        booking = Booking.objects.create(
            title="Booking 1", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = timezone.now() + datetime.timedelta(days=4)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            False)

    def test_is_available_with_end_eq_booking_start(self):
        """
        is_available() returns True for spaceship with a booking starting 
        exactly when the specified date time range ends.
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now() + datetime.timedelta(days=2)
        booking_to_date = timezone.now() + datetime.timedelta(days=3)
        booking = Booking.objects.create(
            title="Booking 1", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = timezone.now() + datetime.timedelta(days=1)
        to_date = booking_from_date
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            True)

    def test_is_available_with_start_eq_booking_end(self):
        """
        is_available() returns True for spaceship with a booking ending 
        exactly when the specified date time range starts.
        """
        ship_type = ShipType.objects.create(name="Ship Type 1")
        spaceship = Spaceship.objects.create(
            label="Spaceship 1", 
            ship_type=ship_type, 
            capacity=1)
        booking_from_date = timezone.now()
        booking_to_date = timezone.now() + datetime.timedelta(days=1)
        booking = Booking.objects.create(
            title="Booking 1", 
            spaceship=spaceship, 
            from_date=booking_from_date,
            to_date=booking_to_date)
        from_date = booking_to_date
        to_date = timezone.now() + datetime.timedelta(days=2)
        self.assertIs(
            spaceship.is_available(from_date=from_date, to_date=to_date), 
            True)
