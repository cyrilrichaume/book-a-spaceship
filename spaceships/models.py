import datetime

from django.db import models

class ShipType(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Spaceship(models.Model):
    label = models.CharField(max_length=32)
    ship_type = models.ForeignKey(ShipType, on_delete=models.PROTECT)
    location = models.CharField(max_length=256)
    capacity = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.label

    def is_available(self, from_date, to_date):
        """
        Check if there is already a booking for the given spaceship 
        at a date between the given from_date and to_date.
        """
        # Make sure from_date is before to_date
        if from_date > to_date:
            from_date, to_date = to_date, from_date
        
        bookings = Booking.objects.filter(
            spaceship=self,
            from_date__lt=to_date, 
            to_date__gt=from_date
            ).count()
        if bookings > 0:
            return False
        return True


class NotAvailable(Exception):
    pass

class Booking(models.Model):
    title = models.CharField(max_length=128)
    spaceship = models.ForeignKey(Spaceship, on_delete=models.PROTECT)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # Prevent booking a spaceship if it is not available.
        if not self.spaceship.is_available(self.from_date, self.to_date):
            raise NotAvailable("This spaceship is not available for the given period ({from_date} - {to_date}).".format(from_date=self.from_date, to_date=self.to_date))
        else:
            super().save(*args, **kwargs)
