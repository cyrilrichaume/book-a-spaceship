from django.contrib import admin

from .models import Spaceship, ShipType, Booking

admin.site.register(Spaceship)
admin.site.register(ShipType)
admin.site.register(Booking)