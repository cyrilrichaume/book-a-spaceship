# Generated by Django 2.2.19 on 2021-04-12 14:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ShipType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='SpaceShip',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label', models.CharField(max_length=32)),
                ('location', models.CharField(max_length=256)),
                ('capacity', models.PositiveSmallIntegerField()),
                ('ship_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='spaceships.ShipType')),
            ],
        ),
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('from_date', models.DateTimeField()),
                ('to_date', models.DateTimeField()),
                ('spaceship', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='spaceships.SpaceShip')),
            ],
        ),
    ]
